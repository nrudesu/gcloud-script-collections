#!/bin/sh
# credit : https://cloud.google.com/community/tutorials/create-a-self-deleting-virtual-machine
sleep 8h
export NAME=$(curl -X GET http://metadata.google.internal/computeMetadata/v1/instance/name -H 'Metadata-Flavor: Google')
export ZONE=$(curl -X GET http://metadata.google.internal/computeMetadata/v1/instance/zone -H 'Metadata-Flavor: Google')
gcloud --quite compute disk snapshot $NAME --snapshot-name=$NAME --zone=$ZONE
gcloud --quiet compute instances delete $NAME --zone=$ZONE
